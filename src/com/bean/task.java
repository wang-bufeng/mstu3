package com.bean;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.bean
 * @FileName: task
 * @Author: 峰。
 * @Date: 2023/8/23-15:16
 * @Version: 1.0
 * @Description:
 */
public class task {
    private String task;
    private String teacherAccount;
    private String taskDate;
    private String taskName;

    public task() {
    }

    public task(String task, String teacherAccount, String taskDate, String taskName) {
        this.task = task;
        this.teacherAccount = teacherAccount;
        this.taskDate = taskDate;
        this.taskName = taskName;
    }

    @Override
    public String toString() {
        return "task{" +
                "task='" + task + '\'' +
                ", teacherAccount='" + teacherAccount + '\'' +
                ", taskDate='" + taskDate + '\'' +
                ", taskName='" + taskName + '\'' +
                '}';
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getTeacherAccount() {
        return teacherAccount;
    }

    public void setTeacherAccount(String teacherAccount) {
        this.teacherAccount = teacherAccount;
    }

    public String getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(String taskDate) {
        this.taskDate = taskDate;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
}
