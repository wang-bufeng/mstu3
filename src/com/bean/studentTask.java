package com.bean;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.bean
 * @FileName: studentTask
 * @Author: 峰。
 * @Date: 2023/8/23-17:11
 * @Version: 1.0
 * @Description:
 */
public class studentTask {
    private String taskAccount;
    private String studentAccount;
    private String studentAnswer;
    private int isFinish;
    private int Score;
    private String StudentName;

    public studentTask() {
    }

    public studentTask(String taskAccount, String studentAccount, String studentAnswer, int isFinish, int score,String S) {
        this.taskAccount = taskAccount;
        this.studentAccount = studentAccount;
        this.studentAnswer = studentAnswer;
        this.isFinish = isFinish;
        Score = score;
        this.StudentName=S;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public String getTaskAccount() {
        return taskAccount;
    }

    public void setTaskAccount(String taskAccount) {
        this.taskAccount = taskAccount;
    }

    public String getStudentAccount() {
        return studentAccount;
    }

    public void setStudentAccount(String studentAccount) {
        this.studentAccount = studentAccount;
    }

    public String getStudentAnswer() {
        return studentAnswer;
    }

    public void setStudentAnswer(String studentAnswer) {
        this.studentAnswer = studentAnswer;
    }

    public int getIsFinish() {
        return this.isFinish;
    }

    public void setIsFinish(int isFinish) {
        this.isFinish = isFinish;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }

    @Override
    public String toString() {
        return "studentTask{" +
                "taskAccount='" + taskAccount + '\'' +
                ", studentAccount='" + studentAccount + '\'' +
                ", studentAnswer='" + studentAnswer + '\'' +
                ", isFinish=" + isFinish +
                ", Score=" + Score +
                '}';
    }
}
