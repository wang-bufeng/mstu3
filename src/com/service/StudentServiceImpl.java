package com.service;

import com.Dao.StudentDao;
import com.Dao.StudentDaoImpl;
import com.bean.Person;
import com.bean.studentTask;
import com.bean.task;

import java.util.List;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.service
 * @FileName: StudentServiceImpl
 * @Author: 峰。
 * @Date: 2023/8/22-20:03
 * @Version: 1.0
 * @Description:
 */
public class StudentServiceImpl implements StudentService{
    StudentDao sd = new StudentDaoImpl();
    @Override
    public List<Person> getAllStudent(String userAccount) {
        String sql = "select * from person where userAccount in (select userAccount from userteacher where teacherAccount = '"+userAccount+"')";
        return sd.getStudentList(sql);
    }

    @Override
    public List<task> getAllMyTask(String userAccount) {
        String sql = "select * from task where teacherAccount = '"+userAccount+"'";
        return sd.getAllMyTask(sql);
    }

    public List<studentTask> getAllStuTask(String userAccount) {
        String sql = "select * from studenttask where studentAccount = '"+userAccount+"'";
        return sd.getStuTask(sql);
    }
    @Override
    public List<studentTask> getTaskDetail(String taskAccount) {
        String sql = "select * from studenttask,person where studenttask.studentAccount=person.userAccount and taskAccount='"+taskAccount+"';";
        return sd.getTaskDetail(sql);
    }

    @Override
    public int upScore(int upScore, String taskAccount) {
        String sql = "update studenttask set Score = "+upScore+" where taskAccount = '"+taskAccount+"';";

        return sd.upScore(sql);
    }

    @Override
    public int DelTask(String taskAccount) {
        String sql = "delete from studenttask where taskAccount = '"+taskAccount+"'";
        String sql1 = "delete from task where taskAccount = '"+taskAccount+"'";
        sd.upScore(sql);
        sd.upScore(sql1);
        return 0;
    }

    @Override
    public void upName(String upName,String userName) {
        String sql = "update person set userName = '"+upName+"' where userAccount = '"+userName+"'";
        sd.upScore(sql);
    }

    @Override
    public void upSex(String upSex,String userName) {
        String sql = "update person set userSex = '"+upSex+"' where userAccount = '"+userName+"'";
        sd.upScore(sql);
    }

    @Override
    public void upBirthday(String upBirthday,String userName) {
        String sql = "update person set userBirthday = '"+upBirthday+"' where userAccount = '"+userName+"'";
        sd.upScore(sql);
    }

    @Override
    public void upOtherName(String upOtherName,String userName) {
        String sql = "update person set userOtherName = '"+upOtherName+"' where userAccount = '"+userName+"'";
        sd.upScore(sql);
    }

    @Override
    public List<Person> getMySelf(String userAccount) {
        String sql = "select * from person where userAccount = '"+userAccount+"'";
        return sd.getMySelf(sql);
    }

    @Override
    public List<Person> getAllPersonn(String userAccount) {
        String sql = "select * from person";
        return sd.getStudentList(sql);
    }

    @Override
    public void upAnswer(String answer,String taskAccount) {
        String sql = "update studenttask set studentAnswer = '"+answer+"' where taskAccount = "+taskAccount+"";
        sd.upAnswer(sql);
    }

    @Override
    public List<studentTask> getStuTaskByTask(String taskAccount) {
        String sql = "select * from studenttask where taskAccount = '"+taskAccount+"'";
        return sd.getStuTask(sql);
    }
}
