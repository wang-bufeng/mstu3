package com.service;

import com.bean.Person;

public interface UserLoginService {
    Person getLogin(Person person);
}
