package com.service;

import com.Dao.ManageDao;
import com.Dao.ManageDaoImpl;
import com.bean.Person;
import com.bean.task;

import java.util.List;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.service
 * @FileName: ManageServiceImpl
 * @Author: 峰。
 * @Date: 2023/8/22-20:04
 * @Version: 1.0
 * @Description:
 */
public class ManageServiceImpl implements ManageService{

    ManageDao md =  new ManageDaoImpl();
    @Override
    public List<Person> getAllPerson() {

        return md.getAllPerson();
    }

    @Override
    public List<task> getAllTask() {
        String sql = "select * from task";
        return md.getAllTask(sql);
    }
}
