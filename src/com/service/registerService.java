package com.service;

import com.bean.Person;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.service
 * @FileName: registerService
 * @Author: 峰。
 * @Date: 2023/8/24-18:15
 * @Version: 1.0
 * @Description:
 */
public interface registerService {
    int addUser(Person person);
}
