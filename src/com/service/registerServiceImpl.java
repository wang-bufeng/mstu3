package com.service;

import com.Dao.registerDao;
import com.Dao.registerDaoImpl;
import com.bean.Person;

import java.util.Random;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.service
 * @FileName: registerServiceImpl
 * @Author: 峰。
 * @Date: 2023/8/24-18:16
 * @Version: 1.0
 * @Description:
 */
public class registerServiceImpl implements registerService{
    registerDao rd =  new registerDaoImpl();
    @Override
    public int addUser(Person person) {
        person.setUserIdCard(RandomIdCard());
        String sql = "insert into person (userAccount,userBirthday,userIdCard,userIdentify,userName,userOtherName,userPassword,userSex) \n" +
                "values ('"+person.getUserAccount()+"','"+person.getUserBirthday()+"','"+person.getUserIdCard()+"'," +
                ""+person.getUserIdentify()+",'"+person.getUserName()+"','"+person.getUserOtherName()+"','"+person.getUserPassword()+"'" +
                ",'"+person.getUserSex()+"')";
        return rd.addUser(sql);
    }

    public String RandomIdCard(){
        Random random = new Random();
        String idcard = "";
        for (int i = 0 ;i<9;i++) {
            String randomNumber = String.valueOf(random.nextInt(10));
            System.out.println("随机数: " + randomNumber);
            idcard = idcard+randomNumber;
        }
        System.out.println(idcard);
        return idcard;
    }
}
