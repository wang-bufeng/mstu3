package com.service;

import com.Dao.TestDAO;
import com.bean.Person;

/**
 * @BelongsProject: MSTU1
 * @BelongsPackage: com.service
 * @FileName: UserLoginServiceImpl
 * @Author: 峰。
 * @Date: 2023/8/21-20:13
 * @Version: 1.0
 * @Description:
 */
public class UserLoginServiceImpl implements UserLoginService{
    @Override
    public Person getLogin(Person person) {
        Person person1 = TestDAO.Login(person);
        return person1;
    }
}
