package com.service;

import com.bean.Person;
import com.bean.studentTask;
import com.bean.task;

import java.util.List;

public interface StudentService {
    List<Person> getAllStudent(String userAccount);

    List<task> getAllMyTask(String userAccount);


    List<studentTask> getTaskDetail(String taskAccount);
    public List<studentTask> getAllStuTask(String userAccount);
    int upScore(int upScore, String taskAccount);

    int DelTask(String taskAccount);

    void upName(String upName,String userAccount);

    void upSex(String upSex,String userAccount);

    void upBirthday(String upBirthday,String userAccount);

    void upOtherName(String upOtherName,String userAccount);

    List<Person> getMySelf(String userAccount);

    List<Person> getAllPersonn(String userAccount);

    void upAnswer(String answer,String taskAccount);

    List<studentTask> getStuTaskByTask(String taskAccount);
}
