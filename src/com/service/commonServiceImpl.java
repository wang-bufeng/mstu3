package com.service;

import com.Dao.commonDao;
import com.Dao.commonDaoImpl;
import com.bean.Person;
import com.bean.studentTask;

import java.util.List;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.service
 * @FileName: commonServiceImpl
 * @Author: 峰。
 * @Date: 2023/8/26-14:33
 * @Version: 1.0
 * @Description:
 */
public class commonServiceImpl implements commonService{
    commonDao cd = new commonDaoImpl();
    @Override
    public void deletePerson(String userAccount) {
        String sql = "delete from person where userAccount = '"+userAccount+"'";
        cd.deletePerson(sql);
    }

    @Override
    public List<Person> getAllTea() {
        String sql = "select * from person where userIdentify = 0";
        return cd.getAllTea(sql);
    }

    @Override
    public void selectPash(String userAccount, String Account) {
        String sql = "insert into userteacher(userAccount,teacherAccount) values('"+Account+"','"+userAccount+"')";
        cd.selectTea(sql);
    }

    @Override
    public List<studentTask> selectTaskDetail(String taskAccount) {
        String sql = "select * from studenttask where taskAccount = "+taskAccount+"";
        return cd.getMyTask(sql);
    }

}
