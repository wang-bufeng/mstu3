package com.service;

import com.bean.Person;
import com.bean.task;

import java.util.List;

public interface ManageService {
    List<Person> getAllPerson();

    List<task> getAllTask();
}
