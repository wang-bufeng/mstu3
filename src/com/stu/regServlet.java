package com.stu;

import com.bean.Person;
import com.service.registerService;
import com.service.registerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.stu
 * @FileName: regServlet
 * @Author: 峰。
 * @Date: 2023/8/24-18:11
 * @Version: 1.0
 * @Description:
 */
@WebServlet("/regServlet")
public class regServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        registerService rs = new registerServiceImpl();
        Person person = new Person();
        person.setUserAccount(req.getParameter("userAccount"));
        person.setUserPassword(req.getParameter("userPassword"));
        person.setUserBirthday(req.getParameter("userBirthday"));
        person.setUserIdentify(1);
        person.setUserName(req.getParameter("userName"));
        person.setUserSex(req.getParameter("userSex"));
        person.setUserOtherName(req.getParameter("userOtherName"));
        System.out.println(person.toString());
        int bool = rs.addUser(person);
        if (bool == 1){
        req.getRequestDispatcher("UserLogin.jsp").forward(req,resp);
        }else {
            req.setAttribute("registerFalse","注册失败");
            req.getRequestDispatcher("UserLogin.jsp").forward(req,resp);
        }
    }
}
