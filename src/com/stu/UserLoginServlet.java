package com.stu;

import com.Dao.TestDAO;
import com.bean.Person;
import com.service.UserLoginService;
import com.service.UserLoginServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @BelongsProject: MSTU1
 * @BelongsPackage: com.stu
 * @FileName: UserLoginServlet
 * @Author: 峰。
 * @Date: 2023/8/20-19:25
 * @Version: 1.0
 * @Description:
 */
@WebServlet("/UserLogin")
public class UserLoginServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        System.out.println("进来了");
        //1.拿到前端传递过来的信息，比方说用户名和密码
        String username = req.getParameter("userAccount");
        String password = req.getParameter("userPassword");
        Person person = new Person(username,password);
//        Person person1 = TestDAO.Login(person);
        UserLoginService uls = new UserLoginServiceImpl();
        Person person1 = uls.getLogin(person);
//        if (username.equals("admin") && password.equals("word")){
        if (username.equals(person1.getUserAccount())&&password.equals(person1.getUserPassword())){
//            进入主页面
            System.out.println(person1);

            req.getSession().setAttribute("session_person",person1);//默认保存30分钟
//            req.setAttribute("person",person1);
            req.setAttribute("mainRight","blank.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        }else {
//            否则，跳回登录界面
            req.setAttribute("tip","账户密码错误");
            req.getRequestDispatcher("UserLogin.jsp").forward(req,resp);//转跳回界面
        }
    }
}
