package com.stu;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.stu
 * @FileName: OutLoginServlet
 * @Author: 峰。
 * @Date: 2023/8/22-21:18
 * @Version: 1.0
 * @Description:
 */
@WebServlet("/outLogin")
public class OutLoginServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().getAttribute("session_person");
        HttpSession session = req.getSession();
        session.removeAttribute("session_person");
        req.getRequestDispatcher("UserLogin.jsp").forward(req,resp);
    }
}
