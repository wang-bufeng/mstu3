package com.stu;

import com.bean.Person;
import com.bean.task;
import com.service.ManageService;
import com.service.ManageServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.stu
 * @FileName: ManageServlet
 * @Author: 峰。
 * @Date: 2023/8/22-20:00
 * @Version: 1.0
 * @Description:
 */
@WebServlet("/man.action")
public class ManageServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String action = req.getParameter("action");
        ManageService ms = new ManageServiceImpl();
        if (action.equals("list")){
            List<Person> personList = ms.getAllPerson();
            for (Person p :personList){
                System.out.println(p.toString());
            }
            req.setAttribute("arr",personList);
            req.setAttribute("mainRight","person.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        }else if (action.equals("myThing")) {
            Person p = (Person) req.getSession().getAttribute("session_person");
            List<Person> personList= new ArrayList<>();
            personList.add(p);
            req.setAttribute("arr",personList);
            req.setAttribute("mainRight","SelfThing.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("taskList")) {
            Person p = (Person) req.getSession().getAttribute("session_person");
            List<task> taskList =  ms.getAllTask();
            for (task t:taskList){
                System.out.println(t.toString());
            }
            req.setAttribute("arr",taskList);
            req.setAttribute("mainRight","task.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        }
    }
}
