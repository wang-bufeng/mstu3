package com.stu;

import com.bean.Person;
import com.bean.studentTask;
import com.bean.task;
import com.service.StudentService;
import com.service.StudentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.stu
 * @FileName: TeacherServlet
 * @Author: 峰。
 * @Date: 2023/8/22-19:59
 * @Version: 1.0
 * @Description:
 */
@WebServlet("/tea.action")
public class TeacherServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String action = req.getParameter("action");
        StudentService ss = new StudentServiceImpl();
//        Person session_person = (Person) req.getAttribute("session_person");直接获取拿不到person对象,需要通过session获取
        Person session_person = (Person) req.getSession().getAttribute("session_person");
        if (action.equals("list")){
            List<Person> personList = ss.getAllStudent(session_person.getUserAccount());
            //获取学生列表
            for (Person p:personList){
                System.out.println(p.toString());
            }
            req.setAttribute("arr",personList);
            req.setAttribute("mainRight","person.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("goMyTask")) {
            List<task> taskList = ss.getAllMyTask(session_person.getUserAccount());
            for (task t:taskList){
                System.out.println(t.toString());
            }
            req.setAttribute("arr",taskList);
            req.setAttribute("mainRight","task.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);

        } else if (action.equals("goSkimDetail")) {
            //查看任务
            String taskAccount = req.getParameter("taskAccount");
            List<studentTask> taskList= ss.getTaskDetail(taskAccount);
            req.setAttribute("arr",taskList);
            req.setAttribute("mainRight","studentTask.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("myThing")) {
            List<Person> personList = ss.getMySelf(session_person.getUserAccount());
            req.setAttribute("arr",personList);
            req.setAttribute("mainRight","SelfThing.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("upScoree")) {
            String taskAccount = req.getParameter("taskAccount");
            int upScore = Integer.parseInt(req.getParameter("Score"));
            int res = ss.upScore(upScore,taskAccount);
            if (res == 1){
            List<studentTask> taskList= ss.getTaskDetail(taskAccount);
            req.setAttribute("arr",taskList);
            req.setAttribute("mainRight","studentTask.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
            }
        } else if (action.equals("upScore")) {
            String taskAccount = req.getParameter("taskAccount");
            List<studentTask> taskList= ss.getTaskDetail(taskAccount);
            req.setAttribute("arr",taskList);
            req.setAttribute("mainRight","upScore.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("Del")) {
            String taskAccount = req.getParameter("taskAccount");
            int res = ss.DelTask(taskAccount);
                List<task> taskList = ss.getAllMyTask(session_person.getUserAccount());
                for (task t : taskList) {
                    System.out.println(t.toString());
                }
                req.setAttribute("arr", taskList);
                req.setAttribute("mainRight", "task.jsp");
                req.getRequestDispatcher("main.jsp").forward(req, resp);
        } else if (action.equals("upSelf")) {
            Person p = (Person) req.getSession().getAttribute("session_person");
            List<Person> personList= new ArrayList<>();
            personList.add(p);
            req.setAttribute("arr",personList);
            req.setAttribute("mainRight","upSelf.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("upSelfName")) {
            System.out.println("111");
            String upName = req.getParameter("userName");
            String userAccount = req.getParameter("userAccount");
            System.out.println(upName);
            ss.upName(upName,userAccount);
            List<Person> p = ss.getMySelf(userAccount);
            req.setAttribute("arr",p);
            req.setAttribute("mainRight","upSelf.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("upSelfSex")) {

            String upSex = req.getParameter("userSex");
            String userAccount = req.getParameter("userAccount");
            ss.upSex(upSex,userAccount);
            List<Person> p = ss.getMySelf(userAccount);
            req.setAttribute("arr",p);
            req.setAttribute("mainRight","upSelf.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("upSelfBirthday")) {

            String upBirthday = req.getParameter("userBirthday");
            String userAccount = req.getParameter("userAccount");
            ss.upBirthday(upBirthday,userAccount);
            List<Person> p = ss.getMySelf(userAccount);
            req.setAttribute("arr",p);
            req.setAttribute("mainRight","upSelf.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        }else if (action.equals("upSelfOtherName")){

            String upOtherName = req.getParameter("OtherName");
            String userAccount = req.getParameter("userAccount");
            ss.upOtherName(upOtherName,userAccount);
            List<Person> p = ss.getMySelf(userAccount);
            req.setAttribute("arr",p);
            req.setAttribute("mainRight","upSelf.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        }
    }
}
