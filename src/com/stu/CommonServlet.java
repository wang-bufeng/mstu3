package com.stu;

import com.bean.Person;
import com.bean.studentTask;
import com.bean.task;
import com.service.StudentService;
import com.service.StudentServiceImpl;
import com.service.commonService;
import com.service.commonServiceImpl;
import javafx.concurrent.Task;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.stu
 * @FileName: CommonServlet
 * @Author: 峰。
 * @Date: 2023/8/22-19:51
 * @Version: 1.0
 * @Description:
 */
@WebServlet("/common.action")
public class CommonServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String action = req.getParameter("action");
        commonService sc = new commonServiceImpl();
        StudentService ss = new StudentServiceImpl();
        if (action.equals("index")){
            //转跳到首页
            req.setAttribute("mainRight","blank.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("myThing")) {
            Person p = (Person) req.getSession().getAttribute("session_person");
            List<Person> personList= new ArrayList<>();
            personList.add(p);
            req.setAttribute("arr",personList);
            req.setAttribute("mainRight","SelfThing.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("deletePerson")) {
            List<Person> personList = new ArrayList<>();
            String userAccount = req.getParameter("personAccount");
            sc.deletePerson(userAccount);
            Person session_person = (Person) req.getSession().getAttribute("session_person");
            if (session_person.getUserIdentify() == 0){
            personList = ss.getAllStudent(session_person.getUserAccount());
            } else if (session_person.getUserIdentify() == 2) {
                personList = ss.getAllPersonn(session_person.getUserAccount());
            }
            //获取学生列表
            for (Person p:personList){
                System.out.println(p.toString());
            }
            req.setAttribute("arr",personList);
            req.setAttribute("mainRight","person.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("upStudent")) {
            String studentAccount = req.getParameter("student");
            List<Person> personList = ss.getMySelf(studentAccount);
            //获取学生列表
            for (Person p:personList){
                System.out.println(p.toString());
            }
            req.setAttribute("arr",personList);
            req.setAttribute("mainRight","upPerson.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("upPersonName")) {
            String upName = req.getParameter("userName");
            String userAccount = req.getParameter("userAccount");
            ss.upName(upName,userAccount);
            List<Person> p = ss.getMySelf(userAccount);
            req.setAttribute("arr",p);
            req.setAttribute("mainRight","upPerson.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        }else if (action.equals("upPersonSex")) {
            String upSex = req.getParameter("userSex");
            String userAccount = req.getParameter("userAccount");
            ss.upSex(upSex,userAccount);
            List<Person> p = ss.getMySelf(userAccount);
            req.setAttribute("arr",p);
            req.setAttribute("mainRight","upPerson.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        }else if (action.equals("upPersonBirthday")) {
            String upBirthday = req.getParameter("userBirthday");
            String userAccount = req.getParameter("userAccount");
            ss.upBirthday(upBirthday,userAccount);
            List<Person> p = ss.getMySelf(userAccount);
            req.setAttribute("arr",p);
            req.setAttribute("mainRight","upPerson.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        }else if (action.equals("upPersonOtherName")) {
            String upOtherName = req.getParameter("OtherName");
            String userAccount = req.getParameter("userAccount");
            ss.upOtherName(upOtherName,userAccount);
            List<Person> p = ss.getMySelf(userAccount);
            req.setAttribute("arr",p);
            req.setAttribute("mainRight","upPerson.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("list")) {
            Person session_person = (Person) req.getSession().getAttribute("session_person");
            List<Person> personList = ss.getAllPersonn(session_person.getUserAccount());
            //获取学生列表
            for (Person p:personList){
                System.out.println(p.toString());
            }
            req.setAttribute("arr",personList);
            req.setAttribute("mainRight","person.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("selectTeacher")) {
            Person session_person = (Person) req.getSession().getAttribute("session_person");
            List<Person> teaList = sc.getAllTea();
            for (Person p:teaList){
                System.out.println(p.toString());
            }
            req.setAttribute("arr",teaList);
            req.setAttribute("mainRight","teaList.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("MyTask")) {
            Person session_person = (Person) req.getSession().getAttribute("session_person");
            String userAccount = session_person.getUserAccount();
            List<studentTask> allMyTask =  ss.getAllStuTask(userAccount);
            for (studentTask t:allMyTask){
                System.out.println(t.toString());
            }
            req.setAttribute("arr",allMyTask);
            req.setAttribute("mainRight","taskDetail.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("selectPash")) {
            Person session_person = (Person) req.getSession().getAttribute("session_person");
            String userAccount = req.getParameter("userAccount");
            sc.selectPash(userAccount,session_person.getUserAccount());
            req.setAttribute("mainRight","blank.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("goSkimDetail")) {
            String taskAccount = req.getParameter("taskAccount");
            List<studentTask> tasks = sc.selectTaskDetail(taskAccount);
            req.getSession().setAttribute("task",tasks.get(0));
            req.setAttribute("arr",tasks);
            req.setAttribute("mainRight","taskDetail.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("upAnswer")) {
            String taskAccount = req.getParameter("taskAccount");
            List<studentTask> tasks = ss.getStuTaskByTask(taskAccount);
            req.setAttribute("arr",tasks);
            req.setAttribute("mainRight","upAnswer.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        } else if (action.equals("answer")) {
            String answer = req.getParameter("answer");
            String taskAccount = req.getParameter("taskAccount");
            ss.upAnswer(answer,taskAccount);
            List<studentTask> tasks = ss.getStuTaskByTask(taskAccount);
            req.getSession().setAttribute("task",tasks.get(0));
            req.setAttribute("arr",tasks);
            req.setAttribute("mainRight","taskDetail.jsp");
            req.getRequestDispatcher("main.jsp").forward(req,resp);
        }
    }
}
