package com.DBUtiles;

import com.Dao.TestDAO;
import com.bean.Person;

/**
 * @BelongsProject: MSTU1
 * @BelongsPackage: com.DBUtiles
 * @FileName: Test
 * @Author: 峰。
 * @Date: 2023/8/21-19:53
 * @Version: 1.0
 * @Description:
 */
public class Test {
    public static void main(String[] args) {
        Person p = new Person();
        p.setUserAccount("wbf");
        p.setUserPassword("123");
        Person person = TestDAO.Login(p);
        if (person == null){
            System.out.println("登录失败");
        }else {
            System.out.println("登录成功"+person.toString());
        }
    }
}
