package com.DBUtiles;

import java.sql.*;

/**
 * @BelongsProject: Tab8
 * @BelongsPackage: com.JDBCConnect.www
 * @FileName: DBUtils
 * @Author: 峰。
 * @Date: 2023/5/9-23:27
 * @Version: 1.0
 * @Description: JDBC工具类,两个方法，1)获取连接，2)释放资源
 */
public class DBUtils {
    //1)建立连接
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/db_it1?useSSL=false";
        String user = "root";
        String password = "";
        Connection connection = (Connection) DriverManager.getConnection(url, user, password);
        System.out.println("数据库连接成功~");
        return connection;
    }

    //2)释放资源
    public static void ClearAll(Connection connection, Statement statement, ResultSet resultSet) throws SQLException {
        //空值不能关闭  先开后关
        if (resultSet!=null){
            resultSet.close();
        }
        if (statement!=null){
            statement.close();
        }
        if (connection!=null){
            connection.close();
        }
    }
}
