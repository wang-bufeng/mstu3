package com.Dao;

import com.DBUtiles.DBUtils;
import com.bean.Person;
import com.bean.studentTask;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.Dao
 * @FileName: registerDaoImpl
 * @Author: 峰。
 * @Date: 2023/8/24-18:16
 * @Version: 1.0
 * @Description:
 */
public class registerDaoImpl implements registerDao {
    @Override
    public int addUser(String sql) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int res = 0;
        List<studentTask> personLsit = new ArrayList<>();
        try {
            connection = DBUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            res = preparedStatement.executeUpdate(sql);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (res == 1) {
            System.out.println("添加成功");
            return res;
        } else {
            System.out.println("添加失败");
            return res;
        }
    }
}
