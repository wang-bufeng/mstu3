package com.Dao;

import com.DBUtiles.DBUtils;
import com.bean.Person;
import com.bean.studentTask;
import com.bean.task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.Dao
 * @FileName: commonDaoImpl
 * @Author: 峰。
 * @Date: 2023/8/26-14:33
 * @Version: 1.0
 * @Description:
 */
public class commonDaoImpl implements commonDao{
    @Override
    public void deletePerson(String sql) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int res = 0;
        List<studentTask> personLsit = new ArrayList<>();
        try {
            connection = DBUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            res = preparedStatement.executeUpdate();
            DBUtils.ClearAll(connection,preparedStatement,resultSet);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Person> getAllTea(String sql) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Person> personLsit = new ArrayList<>();
        try {
            connection = DBUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                Person person = new Person();
                person.setUserName(resultSet.getString("userName"));
                person.setUserSex(resultSet.getString("userSex"));
                person.setUserIdCard(resultSet.getString("userIdCard"));
                person.setUserOtherName(resultSet.getString("userOtherName"));
                person.setUserIdentify(resultSet.getInt("userIdentify"));
                person.setUserAccount(resultSet.getString("userAccount"));
                person.setUserPassword(resultSet.getString("userPassword"));
                person.setUserBirthday(resultSet.getString("userBirthday"));
                personLsit.add(person);
            }
            DBUtils.ClearAll(connection,preparedStatement,resultSet);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return personLsit;
    }

    @Override
    public void selectTea(String sql) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int res = 0;
        try {
            connection = DBUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            res = preparedStatement.executeUpdate();
            DBUtils.ClearAll(connection,preparedStatement,resultSet);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<studentTask> getMyTask(String sql) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<studentTask> personLsit = new ArrayList<>();
        try {
            connection = DBUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                studentTask st = new studentTask();
                st.setIsFinish(resultSet.getInt("isFinish"));
                st.setScore(resultSet.getInt("Score"));
                st.setStudentAccount(resultSet.getString("StudentAccount"));
                st.setTaskAccount(resultSet.getString("taskAccount"));
                st.setStudentName(resultSet.getString("userName"));
                st.setStudentAnswer(resultSet.getString("studentAnswer"));
                personLsit.add(st);
            }
            DBUtils.ClearAll(connection,preparedStatement,resultSet);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return personLsit;
    }
}
