package com.Dao;

import com.DBUtiles.DBUtils;
import com.bean.Person;
import com.bean.task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.Dao
 * @FileName: ManageDaoImpl
 * @Author: 峰。
 * @Date: 2023/8/22-20:11
 * @Version: 1.0
 * @Description:
 */
public class ManageDaoImpl implements ManageDao{
    @Override
    public List<Person> getAllPerson() {
        String sql = "select * from person where userIdentify != 2";
        return getAPerson(sql);
    }

    @Override
    public List<task> getAllTask(String sql) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<task> personLsit = new ArrayList<>();
        try {
            connection = DBUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                task task = new task();
                task.setTask(resultSet.getString("taskAccount"));
                task.setTaskDate(resultSet.getString("taskDate"));
                task.setTaskName(resultSet.getString("taskName"));
                task.setTeacherAccount(resultSet.getString("teacherAccount"));
                personLsit.add(task);
            }
            DBUtils.ClearAll(connection,preparedStatement,resultSet);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return personLsit;
    }

    private List<Person> getAPerson(String sql) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Person> personLsit = new ArrayList<>();
        try {
            connection = DBUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                Person person = new Person();
                person.setUserName(resultSet.getString("userName"));
                person.setUserSex(resultSet.getString("userSex"));
                person.setUserIdCard(resultSet.getString("userIdCard"));
                person.setUserOtherName(resultSet.getString("userOtherName"));
                person.setUserIdentify(resultSet.getInt("userIdentify"));
                person.setUserAccount(resultSet.getString("userAccount"));
                person.setUserPassword(resultSet.getString("userPassword"));
                person.setUserBirthday(resultSet.getString("userBirthday"));
                personLsit.add(person);
            }

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return personLsit;
    }
}
