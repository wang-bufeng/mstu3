package com.Dao;

import com.bean.Person;
import com.bean.task;
import com.service.ManageServiceImpl;

import java.util.List;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.Dao
 * @FileName: ManageDao
 * @Author: 峰。
 * @Date: 2023/8/22-20:04
 * @Version: 1.0
 * @Description:
 */
public interface ManageDao  {
    List<Person> getAllPerson();

    List<task> getAllTask(String sql);
}
