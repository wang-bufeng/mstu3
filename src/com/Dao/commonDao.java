package com.Dao;

import com.bean.Person;
import com.bean.studentTask;
import com.bean.task;

import java.util.List;

public interface commonDao {
    void deletePerson(String sql);

    List<Person> getAllTea(String sql);

    void selectTea(String sql);

    List<studentTask> getMyTask(String sql);
}
