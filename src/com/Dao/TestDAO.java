package com.Dao;

import com.DBUtiles.DBUtils;
import com.bean.Person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @BelongsProject: MSTU1
 * @BelongsPackage: com.DBUtiles
 * @FileName: TestDAO
 * @Author: 峰。
 * @Date: 2023/8/21-19:51
 * @Version: 1.0
 * @Description:测试数据库连接
 */
public class TestDAO {

    public static Person Login(Person p) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Person person = new Person();
        String sql = "select * from person where userAccount ='"+p.getUserAccount()+"' and userPassword ="+p.getUserPassword()+";";
        try {
             connection = DBUtils.getConnection();
             preparedStatement = connection.prepareStatement(sql);
             resultSet = preparedStatement.executeQuery();
             while(resultSet.next()){
                 person.setUserName(resultSet.getString("userName"));
                 person.setUserSex(resultSet.getString("userSex"));
                 person.setUserIdCard(resultSet.getString("userIdCard"));
                 person.setUserOtherName(resultSet.getString("userOtherName"));
                 person.setUserIdentify(resultSet.getInt("userIdentify"));
                 person.setUserAccount(resultSet.getString("userAccount"));
                 person.setUserPassword(resultSet.getString("userPassword"));
                 person.setUserBirthday(resultSet.getString("userBirthday"));
             }

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return person;
    }
}
