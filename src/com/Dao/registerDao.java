package com.Dao;

import com.bean.Person;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.Dao
 * @FileName: registerDao
 * @Author: 峰。
 * @Date: 2023/8/24-18:16
 * @Version: 1.0
 * @Description:
 */
public interface registerDao {
    int addUser(String sql);
}
