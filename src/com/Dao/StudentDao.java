package com.Dao;

import com.bean.Person;
import com.bean.studentTask;
import com.bean.task;

import java.util.List;

/**
 * @BelongsProject: MUST3
 * @BelongsPackage: com.Dao
 * @FileName: StudentDao
 * @Author: 峰。
 * @Date: 2023/8/22-20:04
 * @Version: 1.0
 * @Description:
 */
public interface StudentDao {
    List<Person> getStudentList(String sql);

    List<task> getAllMyTask(String sql);

    List<studentTask> getTaskDetail(String sql);

    int upScore(String sql);

    List<Person> getMySelf(String sql);

    void upAnswer(String sql);

    List<studentTask> getStuTask(String sql);
}
