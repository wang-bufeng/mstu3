<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        table{
            width: 1200px;
        }
        tr,td{
            border: grey 1px ;
        }
        td{
            height: 40px;
        }

        table td{
            height: 60px;
            vertical-align: middle!important;  /*设置文字垂直居中*/
            text-align: center;
        }

        tr,td{border-right:1px solid #888;border-bottom:1px solid #888;padding:5px 15px;}

        th{font-weight:bold;background:#ccc;}

        .con-b .row .lf{
            width: 15%;
            text-align: center;
            padding: 10px;
        }
        .con-b .row .rg{
            width: 85%;
        }

        .con-b tr:nth-of-type(odd){
            background-color: #f2f2f2;
        }
        #ee{
            width: 73px;
            height: 44px;
        }

    </style>
    <%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

</head>
<body>
<table border="1" cellspacing="0" align="center" class="con-b">
    <thead>
    <tr class="row" style="color: black;font-size: 25px" >
        <td class="lf">账户</td>
        <td class="lf">姓名</td>
        <td class="lf">性别</td>
        <td class="lf">生日</td>
        <td class="lf">身份证号码</td>
        <td class="lf">角色</td>
        <td class="lf">其他名称</td>
        <td class="lf">提交</td>
    </tr>
    </thead>
    <tbody>

    <tr>
        <c:forEach items="${arr}" var="person">
        <td class="lf">${person.getUserAccount()}</td>
        <td class="lf">
           <form  action="common.action?action=upPersonName&userAccount=${person.getUserAccount()}" method="post">
                <input placeholder="${person.getUserName()}" type="text"  name="userName" class="lf">
                <input type="submit" style="width: 80px ;margin-left :40px;margin-left: 5px" onclick="return upThisName()" value="提交">

            </form>


        <td class="lf">
           <form  action="common.action?action=upPersonSex&userAccount=${person.getUserAccount()}" method="post">
                <input placeholder="${person.getUserSex()}" type="text"  name="userSex" class="lf">
                <input type="submit" style="width: 80px ;margin-left :40px;margin-left: 5px" onclick="return upThisSex()" value="提交">

            </form>

        </td>
        <td class="lf">
          <form action="common.action?action=upPersonBirthday&userAccount=${person.getUserAccount()}" method="post">
                <input placeholder="${person.getUserBirthday()}" type="text"  name="userBirthday" class="lf">
                <input type="submit" style="width: 80px ;margin-left :40px;margin-left: 5px" onclick="return upThisBirthday(${person.getUserIdentify()})" value="提交">

            </form>

        </td>
        <td class="lf">${person.getUserIdCard()}</td>
        <td class="lf">
            <c:if test="${person.getUserIdentify()==1}">学生</c:if>
            <c:if test="${person.getUserIdentify()==0}">老师</c:if>
        </td>
        <td class="lf">
           <form  action="common.action?action=upPersonOtherName&userAccount=${person.getUserAccount()}" method="post">
                <input placeholder="${person.getUserOtherName()}" type="text"  name="OtherName" class="lf">
                <input type="submit" style="width: 80px ;margin-left :40px;margin-left: 5px" onclick="return upThisOtherName(${person.getUserIdentify()})" value="提交">

            </form>

        </td>
        <td ><img style="width: 73px;height: 44px" src="tijiao.png" onclick="upThis(${person.getUserIdentify()})"> </td>
    </tr>
    </c:forEach>
    <span style="color:red; font-size: 5px" id="tip" ></span>

    </tr>

    </tbody>

</table>
<script>
    function  upThis(Identify){
            window.location.href="common.action?action=list";
    }
    function upThisName(){
        let userName = document.getElementById("userName").value;
        if (userName == null || userName.trim()==""){
            alert("请正确输入后提交");
            return false;
        }
        // if (Identify == 0){
        //   window.location.href="tea.action?action=upSelfName";
        // }
        // if (Identify == 1){
        //   window.location.href="common.action?action=upSelfName";
        // }
        // if (Identify ==2){
        //   window.location.href="man.action?action=upSelfName";
        // }
    }
    function upThisSex(){
        let userSex = document.getElementById("userSex").value;
        if (userSex == null || userSex.trim()==""){
            alert("请正确输入后提交");
            return false;
        }
        // if (Identify == 0){
        //   window.location.href="tea.action?action=upSelfSex";
        // }
        // if (Identify == 1){
        //   window.location.href="common.action?action=upSelfSex";
        // }
        // if (Identify ==2){
        //   window.location.href="man.action?action=upSelfSex";
        // }
    }
    function upThisBirthday(){
        let userBirthday = document.getElementById("userBirthday").value;
        if (userBirthday == null || userBirthday.trim()==""){
            alert("请正确输入后提交");
            return false;
        }
        // if (Identify == 0){
        //   window.location.href="tea.action?action=upSelfBirthday";
        // }
        // if (Identify == 1){
        //   window.location.href="common.action?action=upSelfBirthday";
        // }
        // if (Identify ==2){
        //   window.location.href="man.action?action=upSelfBirthday";
        // }
    }
    function  upThisOtherName(){
        let userOtherName = document.getElementById("userOtherName").value;
        if (userOtherName == null || userOtherName.trim()==""){
            alert("请正确输入后提交");
            return false;
        }
        // if (Identify == 0){
        //   window.location.href="tea.action?action=upSelfOtherName";
        // }
        // if (Identify == 1){
        //   window.location.href="common.action?action=upSelfOtherName";
        // }
        // if (Identify ==2){
        //   window.location.href="man.action?action=upSelfOtherName";
        // }
    }
</script>
</body>

</html>
