<%--
  Created by IntelliJ IDEA.
  User: 峰。
  Date: 2023/8/23
  Time: 17:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <style>
    table{
      width: 1200px;
    }
    tr,td{
      border: grey 1px ;
    }
    td{
      height: 40px;
    }

    table td{
      height: 60px;
      vertical-align: middle!important;  /*设置文字垂直居中*/
      text-align: center;
    }

    tr,td{border-right:1px solid #888;border-bottom:1px solid #888;padding:5px 15px;}

    th{font-weight:bold;background:#ccc;}

    .con-b .row .lf{
      width: 15%;
      text-align: center;
      padding: 10px;
    }
    .con-b .row .rg{
      width: 85%;
    }

    .con-b tr:nth-of-type(odd){
      background-color: #f2f2f2;
    }
    #ee{
      width: 73px;
      height: 44px;
    }

  </style>
  <%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

</head>
<body>
<table border="1" cellspacing="0" align="center" class="con-b">
  <thead>
  <tr class="row" style="color: black;font-size: 25px" >
    <td class="lf">任务</td>
<%--    <td class="lf">学生账户</td>--%>
<%--    <td class="lf">学生姓名</td>--%>
    <td class="lf">学生答案</td>
    <td class="lf">是否完成</td>
    <td class="lf">分数</td>
    <td class="lf">修改</td>
    <%--        <td>查看评论</td>--%>

    <%--    <td>删除</td>--%>
    <%--    <td>修改</td>--%>
  </tr>
  </thead>
  <tbody>

  <tr>
    <c:forEach items="${arr}" var="taskDetail">
    <td class="lf">${taskDetail.getTaskAccount()}</td>
<%--    <td class="lf">${taskDetail.getStudentAccount()}</td>--%>
<%--    <td class="lf">${taskDetail.getStudentName()}</td>--%>
    <td class="lf">${taskDetail.getStudentAnswer()}</td>
    <td class="lf">
      <c:if test="${taskDetail.getIsFinish()==0}"><span style="color: red">未完成</span></c:if>
      <c:if test="${taskDetail.getIsFinish()==1}"><span style="color: aqua">已完成</span></c:if>
    </td>
    <td class="lf">${taskDetail.getScore()}</td>
    <td class="lf"><img style="width: 83px;height: 44px" src="xiugai.png" onclick="upAnswer(${taskDetail.getTaskAccount()})"> </td>


      <%--    <td class="lf">${person.getUserIdCard()}</td>--%>
      <%--    <td class="lf">--%>
      <%--      <c:if test="${person.getUserIdentify()==0}">学生</c:if>--%>
      <%--      <c:if test="${person.getUserIdentify()==1}">老师</c:if>--%>
      <%--    </td>--%>
      <%--    <td class="lf">${person.getUserOtherName()}</td>--%>
      <%--    <td ><img style="width: 73px;height: 44px" onclick="deleteThis('${person.getUserAccount()}')" src="shanchu.png"> </td>--%>
      <%--    <td ><img style="width: 73px;height: 44px" src="xiugai.png" onclick="upThis('${person.getUserAccount()}')"> </td>--%>
  </tr>
  </c:forEach>

  </tr>

  </tbody>

</table>

</body>
<script>
  function upAnswer(taskAccount){
    window.location.href="common.action?action=upAnswer&taskAccount="+taskAccount;
  }
</script>
<%--<script>--%>
<%--    function skimDetail(taskAccount){--%>
<%--        //查看学生任务完成情况--%>
<%--        window.location.href = "tea.action?action=goSkimDetail&taskAccount="+taskAccount;--%>
<%--    }--%>
<%--</script>--%>
<%--<script>--%>
<%--  function deleteThis(userAccount){--%>
<%--    alert(userAccount);--%>
<%--  }--%>

<%--  function upThis(userAccount){--%>
<%--    alert(userAccount);--%>
<%--  }--%>
<%--</script>--%>
</html>
