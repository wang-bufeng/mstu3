<%--
  Created by IntelliJ IDEA.
  User: 峰。
  Date: 2023/8/20
  Time: 21:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        #BigBodyFrame{
            width: 1500px;
            height: 120px;
            /*margin-left:200px;*/
            border: grey 1px solid;
            background-image: url("VCG211448478478.jpg");
        }

        #daohang{
            width: 200px;
            height: 320px;
            border: grey 1px solid;
            float: left;
        }

        #file{
            width: 180px;
            height: 50px;
            margin-left: 10px;
            margin-top: 10px;
            border: grey 1px solid;
            background-color: antiquewhite;
        }
        a{
            color: black;
            font-size: 26px;
            margin-left: 15px;
            margin-top: 15px;
        }
    </style>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <link href="${pageContext.request.contextPath}/css/table.css" rel="stylesheet" media="screen">

</head>
<body>
   <div id="BigBodyFrame"></div>
   <br>
   <br>
   <span style="float: right; margin-right: 100px; color: grey">当前用户:${session_person.userName}</span>
<%--   <a href="#">修改密码</a>--%>
   <br>

   <div id="daohang">

       <c:if test="${session_person.getUserIdentify()==0}">
           <li id="file"><a href="common.action?action=index">查看首页</a></li>
<%--                                 回到首页的动作--%>
           <li id="file"><a href="tea.action?action=list">学生管理</a></li>
           <li id="file"><a href="tea.action?action=goMyTask">任务管理</a></li>
           <li id="file"><a href="tea.action?action=myThing">我的信息</a></li>
           <li id="file"><a href="outLogin">退出系统</a></li>
       </c:if>

       <c:if test="${session_person.getUserIdentify()==1}">
           <li id="file"><a href="common.action?action=index">查看首页</a></li>
           <li id="file"><a href="common.action?action=selectTeacher">选择老师</a></li>
           <li id="file"><a href="common.action?action=MyTask">我的任务</a></li>
           <li id="file"><a href="common.action?action=myThing">我的信息</a></li>
           <li id="file"><a href="outLogin">退出系统</a></li>
       </c:if>

       <c:if test="${session_person.getUserIdentify()==2}">
           <li id="file"><a href="common.action?action=index">查看首页</a></li>
           <li id="file"><a href="man.action?action=list">人员管理</a></li>
           <li id="file"><a href="man.action?action=taskList">任务管理</a></li>
           <li id="file"><a href="man.action?action=myThing">我的信息</a></li>
           <li id="file"><a href="outLogin">退出系统</a></li>
       </c:if>
<%--       <div id="file" onclick="clickThis()"></div>--%>

   </div>


<jsp:include page="${mainRight = null?'blank.jsp':mainRight}"></jsp:include>
<%--<jsp:include page="${downJSP = null ;downJSP}"></jsp:include>--%>
<%--mainRight是从后端传过来的数据值--%>
</body>
</html>
