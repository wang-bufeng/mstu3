<%--
  Created by IntelliJ IDEA.
  User: 峰。
  Date: 2023/8/24
  Time: 17:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>注册界面</title>
    <style>
        #outerLoginFrame{
            width: 500px;
            height: 600px;
            margin-left: 500px;
            margin-top: 120px;
            border: white 0px solid;
        }

        #innerLoginFrame{
            width: 360px;
            /*border: grey 2px solid;*/
            margin-left: 80px;
            margin-top: 50px;
            height: 250px;
            color: grey;
            font-size: 25px;
        }
        input{
            width: 220px;
            height: 30px;
        }
    </style>

</head>
<body>
<div id="outerLoginFrame" style="border:grey 2px solid">
    <%--      <span style="position: center"></span>--%>
    <h1 style="margin-top: 40px;margin-left: 160px;color: grey">注&nbsp册&nbsp账&nbsp号</h1>
    <div id="innerLoginFrame">
        <form action="regServlet" method="post" onclick=" return checkRegister()">
            账户:<input type="text" id="userAccount" name="userAccount" class="myText"><br><br>
            姓名:<input type="text" id="userName" name="userName" class="myText"><br><br>
            密码:<input type="password" id="userPassword" name="userPassword" class="myText"><br><br>
            性别:<input type="text" id="userSex" name="userSex" class="myText"><br><br>
            生日:<input type="text" id="userBirthday" name="userBirthday" class="myText"><br><br>
            别称:<input type="text" id="userOtherName" name="userOtherName" class="myText"><br><br>
            <input type="submit" style="width: 140px ;margin-left :60px"  value="注册">
            <br>
            <span style="color:red; font-size: 5px; margin-left: 90px" id="tip" ></span>
        </form>
    </div>

</div>





</body>
</html>

<script>
    <%--    方法--%>
    function checkRegister(){
        // alert("进来")  弹窗
        let userAccount = document.getElementById("userAccount").value;
        let userPassword = document.getElementById("userPassword").value;
        let userName = document.getElementById("userName").value;
        let userSex = document.getElementById("userSex").value;
        let userBirthday = document.getElementById("userBirthday").value;
        let userOtherName = document.getElementById("userOtherName").value;
        if (userAccount == null || userAccount.trim()==""){
            // alert("账户不能为空");
            document.getElementById("tip").innerHTML = "账户不能为空";
            return false;
        }
        else if (userPassword == null || userPassword.trim()==""){
            // alert("密码不能为空");
            document.getElementById("tip").innerHTML = "密码不能为空";
            return  false;
        } else if (userName == null || userName.trim()==""){
            // alert("密码不能为空");
            document.getElementById("tip").innerHTML = "请输入名称";
            return  false;
        } else if (userSex == null || userSex.trim()==""){
            // alert("密码不能为空");
            document.getElementById("tip").innerHTML = "请填写性别！";
            return  false;
        } else if (userBirthday == null || userBirthday.trim()==""){
            // alert("密码不能为空");
            document.getElementById("tip").innerHTML = "请填写生日日期!";
            return  false;
        }else if (userOtherName == null || userOtherName.trim()==""){
            // alert("密码不能为空");
            document.getElementById("tip").innerHTML = "请填写别称!";
            return  false;
        }
    }
</script>