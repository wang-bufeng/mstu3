<%--
  Created by IntelliJ IDEA.
  User: 峰。
  Date: 2023/8/18
  Time: 13:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录界面</title>
    <style>
        #outerLoginFrame{
            width: 500px;
            height: 500px;
            margin-left: 500px;
            margin-top: 200px;
            border: white 0px solid;
        }

        #innerLoginFrame{
            width: 360px;
            /*border: grey 2px solid;*/
            margin-left: 80px;
            margin-top: 50px;
            height: 250px;
            color: grey;
            font-size: 25px;
        }
        input{
            width: 220px;
            height: 30px;
        }
    </style>

</head>
<body>
  <div id="outerLoginFrame" style="border:grey 2px solid">
<%--      <span style="position: center"></span>--%>
      <h1 style="margin-top: 40px;margin-left: 120px;color: grey">教&nbsp务&nbsp管&nbsp理&nbsp系&nbsp统</h1>
      <div id="innerLoginFrame">
    <form action="UserLogin" method="post" >
        账户:<input type="text" id="userAccount" name="userAccount" class="myText"><br><br>
        密码:<input type="password" id="userPassword" name="userPassword" class="myText"><br><br>
        <input type="submit" style="width: 140px ;margin-left :40px" onclick="return checkLogin()" value="登录">
        <a href="register.jsp" type="submit" style="width: 140px ;margin-left :20px"  value="注册">|&nbsp&nbsp注册</a><br>
        <span style="color:red; font-size: 5px" id="tip" ></span>
    </form>
    </div>

  </div>





</body>
</html>

<script>
<%--    方法--%>
    function checkLogin(){
         // alert("进来")  弹窗
        let userAccount = document.getElementById("userAccount").value;
        let userPassword = document.getElementById("userPassword").value;
        if (userAccount == null || userAccount.trim()==""){
            // alert("账户不能为空");
            document.getElementById("tip").innerHTML = "账户不能为空";
            return false;
        }
        else if (userPassword == null || userPassword.trim()==""){
            // alert("密码不能为空");
            document.getElementById("tip").innerHTML = "密码不能为空";
            return  false;
        }
    }
</script>
