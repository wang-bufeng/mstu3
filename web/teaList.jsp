<%--
  Created by IntelliJ IDEA.
  User: 峰。
  Date: 2023/8/22
  Time: 19:57
  To change this template use File | Settings | File Templates.
--%>
<%--
  Created by IntelliJ IDEA.
  User: 30890
  Date: 2023/4/17
  Time: 9:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        table{
            width: 1200px;
        }
        tr,td{
            border: grey 1px ;
        }
        td{
            height: 40px;
        }

        table td{
            height: 60px;
            vertical-align: middle!important;  /*设置文字垂直居中*/
            text-align: center;
        }

        tr,td{border-right:1px solid #888;border-bottom:1px solid #888;padding:5px 15px;}

        th{font-weight:bold;background:#ccc;}

        .con-b .row .lf{
            width: 15%;
            text-align: center;
            padding: 10px;
        }
        .con-b .row .rg{
            width: 85%;
        }

        .con-b tr:nth-of-type(odd){
            background-color: #f2f2f2;
        }
        #ee{
            width: 73px;
            height: 44px;
        }

    </style>
    <%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

</head>
<body>
<table border="1" cellspacing="0" align="center" class="con-b">
    <thead>
    <tr class="row" style="color: black;font-size: 25px" >
        <td class="lf">姓名</td>
        <td class="lf">性别</td>
        <td class="lf">角色</td>
        <td class="lf">其他名称</td>
        <td class="lf">选择</td>
    </tr>
    </thead>
    <tbody>

    <tr>
        <c:forEach items="${arr}" var="person">
        <td class="lf">${person.getUserName()}</td>

        <td class="lf">${person.getUserSex()}</td>

        <td class="lf">
            <c:if test="${person.getUserIdentify()==0}">老师</c:if>
            <c:if test="${person.getUserIdentify()==1}">学生</c:if>
            <c:if test="${person.getUserIdentify()==2}">管理员</c:if>
        </td>
        <td class="lf">${person.getUserOtherName()}</td>

        <td ><img style="width: 73px;height: 44px" src="xuanze.png" onclick="selectThis('${person.getUserAccount()}')"> </td>
    </tr>
    </c:forEach>

    </tr>

    </tbody>

</table>

</body>
<script>
   function selectThis(userAccount){
       window.location.href="common.action?action=selectPash&userAccount="+userAccount;
   }
</script>
</html>
