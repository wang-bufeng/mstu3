<%--
  Created by IntelliJ IDEA.
  User: 峰。
  Date: 2023/8/23
  Time: 18:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <style>
    table{
      width: 1200px;
    }
    tr,td{
      border: grey 1px ;
    }
    td{
      height: 40px;
    }

    table td{
      height: 60px;
      vertical-align: middle!important;  /*设置文字垂直居中*/
      text-align: center;
    }

    tr,td{border-right:1px solid #888;border-bottom:1px solid #888;padding:5px 15px;}

    th{font-weight:bold;background:#ccc;}

    .con-b .row .lf{
      width: 15%;
      text-align: center;
      padding: 10px;
    }
    .con-b .row .rg{
      width: 85%;
    }

    .con-b tr:nth-of-type(odd){
      background-color: #f2f2f2;
    }
    #ee{
      width: 73px;
      height: 44px;
    }

  </style>
  <%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

</head>
<body>
<table border="1" cellspacing="0" align="center" class="con-b">
  <thead>
  <tr class="row" style="color: black;font-size: 25px" >
    <td class="lf">账户</td>
    <td class="lf">姓名</td>
    <td class="lf">性别</td>
    <td class="lf">生日</td>
    <td class="lf">身份证号码</td>
    <td class="lf">角色</td>
    <td class="lf">其他名称</td>
    <td class="lf">修改</td>
  </tr>
  </thead>
  <tbody>

  <tr>
    <c:forEach items="${arr}" var="person">
    <td class="lf">${person.getUserAccount()}</td>
    <td class="lf">${person.getUserName()}</td>
    <td class="lf">${person.getUserSex()}</td>
    <td class="lf">${person.getUserBirthday()}</td>
    <td class="lf">${person.getUserIdCard()}</td>
    <td class="lf">
      <c:if test="${person.getUserIdentify()==1}">学生</c:if>
      <c:if test="${person.getUserIdentify()==0}">老师</c:if>
      <c:if test="${person.getUserIdentify()==2}">管理员</c:if>
    </td>
    <td class="lf">${person.getUserOtherName()}</td>
      <td class="lf"><img style="width: 83px;height: 44px" src="xiugai.png" onclick="upThis(${person.getUserIdentify()})"> </td>
  </tr>
  </c:forEach>

  </tr>

  </tbody>

</table>
<script>
  function  upThis(Identify){
    // window.location.href="tea.action?action=upSelf&userAccount="+userAccount;
    // alert(userAccount)
    if (Identify == 0){
      window.location.href="tea.action?action=upSelf";
    }
   if (Identify == 1){
     window.location.href="tea.action?action=upSelf";
   }
   if (Identify == 2){
     window.location.href="tea.action?action=upSelf";
   }
  }
</script>
</body>

</html>
